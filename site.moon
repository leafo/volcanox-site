require "sitegen"
site = sitegen.create_site =>
  @title = "Volcanox"

  deploy_to "leaf@leafo.net", "www/volcanox/"

site\write!
