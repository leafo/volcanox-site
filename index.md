

# Volcanox

Volcanox is a game developed for [Ludum Dare 22][0], the 48 hour game
development contest. The entire game and all the resources were created in 48
hours. The theme of the competition was *alone*. It's fairly short with only one
level.

Volcanox was coded in [MoonScript][1] and runs in the [LÖVE][2] game engine.

The version of the game on this site is as it was submitted, complete with bugs
and all. Maybe in the future I'll improve it.

## Download

**Windows:**

Extract and run the executable: [volcanox.zip][5]

**Other Platforms:**

Install the [LÖVE game engine][2] as directed on their site. Download the
[`.love`][3] file, and open it with the game engine.

## Source

The source is [hosted on GitHub][4].

## Issues

 * sound breaks on windows
 * moving right is faster than moving left


  [0]: http://www.ludumdare.com/compo/
  [1]: http://moonscript.org
  [2]: http://love2d.org
  [3]: ./volcanox.love
  [4]: https://github.com/leafo/ludum-dare-22
  [5]: ./volcanox.zip



